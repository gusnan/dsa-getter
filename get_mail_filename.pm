#!/usr/bin/perl

package get_mail_filename;

use Exporter;

our @ISA = qw ( Exporter );

our @EXPORT = qw( get_mail_filename );

sub get_mail_filename {

   $requested_dsa = $_[0];
   $folder = $_[1];

   my @files;
   opendir(my $DH, $folder) or die "Error opening $DIR: $!";
   while (defined (my $file = readdir($DH))) {
      my $path = $folder . '/' . $file;
      next unless (-f $path);           # ignore non-files - automatically does . and ..

      #ignore files that begin with '.'
      if (substr($file, 0, 1)!='.') {

         # push(@files, [ stat(_), $path ]); # re-uses the stat results from '-f'
         push(@files, $path );
      }
   }
   closedir($DH);

   foreach (@files) {
      $filename = $_;

      my $success = open file, "<", $filename;
      die if ( ! $success );

      while (<file>) {
         $temp = $_;

         $s2 = substr($temp, 0, 29);

         if ($s2 eq 'Debian Security Advisory DSA-') {
            $s = substr($temp, 29, 4);
            if ($s eq $requested_dsa) {
               $s3 = substr($temp, 33, 2);
               if ($s3 eq "-1") {
                  return "$filename";
               }
            }
         }
      }
      close file;
   }
   return $filename;
}
