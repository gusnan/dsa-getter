#!/usr/bin/perl

# Gets a DSA bulletin from a MH Mailbox Format mail folder $folder - interprets
# it and removes what is not needed for the parse-advisory script, runs it
# through parse-advisory, builds the html, opens it in firefox and shows the
# resulting wml and data files in my texteditor of choice SciTE.

# Run this script from the folder webwml/english/security/

use lib ('.');
use get_mail_filename;

$folder = "/home/gusnan/.Mail/inbox/Linux/Debian/Debian\ Security\ Announce";

$requested_dsa = $ARGV[0];

$number = $#ARGV + 1;

if ($number != 1) {
	printf "One argument required, the DSA number\n\n";
	exit;
}

# my $name = `perl ./get_mail_filename.pl $ARGV[0] '$folder'`;

my $name= get_mail_filename($ARGV[0], "$folder");

chomp $name;

my $success = open file, "<", "$name";
if ( ! $success ) {
	printf "Couldn't find '$name'\n";
	die;
}

my $package_name = "";
my $dsa_number = "";

while (<file>) {
	
	$temp = $_;
	
	if ( $temp =~ m/Debian Security Advisory DSA-(?<temp>\w+)/ ) {
		$dsa_number = $+{temp};
	}
	
	if ( $temp =~ m/Package        : (?<temp>\w+)/ ) {
		$package_name = $+{temp};
	}
}

close file;

if ($dsa_number ne $requested_dsa) {
	printf "The latest DSA number $dsa_number doesn't match the requested DSA number $requested_dsa.\n\n";
	exit;
}

$new_filename = "dsa-$dsa_number-1.$package_name";

my $success = open file, "<", "$name";
if ( ! $success) {
	printf "Couldn't open infile $name\n\n";
	exit;
}

# Open the outfile
# printf "Opening $new_filename\n\n";
$success = open outfile, ">", "$new_filename";
if ( ! $success) {
	printf "Couldn't open outfile $name\n\n";
	exit;
}


my $active = 0;

my $year = 0;

while (<file>) {
	
	$temp = $_;
	
	# Start with the line beginning with - ------
	if (substr($temp, 0, 7) eq '- -----') {
		$active = 1;
	}
	
	# Stop at the "We recommend" line
	if (substr($temp, 0, 19) eq 'Further information') {
		$active--;
	}
	
	if ($active > 0) {
		print outfile $temp;
	}
	 
	 if ($temp =~ m/\$w \$w \$w \$w/) {
		 printf "AH: $3\n";
	 }
	 
   $temp2 = substr($temp, 0, -1);
   if (substr($temp2, -12) eq 'security/faq') {
      my @arr = split(/ /, $temp2);
      $year = $arr[2];
   }

}

close file;
close outfile;

$parse_result = `./parse-advisory.pl $new_filename`;

chdir "$year";

`make dsa-$dsa_number.en.html`;

chdir "..";

`firefox-esr $year/dsa-$dsa_number.en.html &`;

`scite $year/dsa-$dsa_number.wml $year/dsa-$dsa_number.data &`;

chdir "$year";
